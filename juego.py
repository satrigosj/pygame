from random import random
import pygame
import random

pygame.init()

screen = pygame.display.set_mode((500,500))
clock=pygame.time.Clock()

running = True

while running:
    
    for event in pygame.event.get():
        
        if event.type == pygame.QUIT:
            running=False
            
    screen.fill((100,0,128))

    pygame.draw.circle(screen,(random.randint(0,255,0)),random.randint((250,250)),random.randint(50))
   
    pygame.display.flip()
    clock.tick(10)

#GUARDAR CAMBIOS
